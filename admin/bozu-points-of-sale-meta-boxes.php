<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link        http://example.com
 * @since       0.0.1-dev
 *
 * @package     Bozu Points of Sale CPT
 * @subpackage  Bozu Points of Sale CPT/admin
 */

/**
 * Adding Meta Box
 *
 * @since       0.0.1-dev
 * @link        https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/#adding-meta-boxes
 * @link        https://developer.wordpress.org/reference/functions/add_meta_box/
 * @link        https://developer.wordpress.org/reference/hooks/add_meta_boxes/
 *
 */
function bposcpt_add_custom_box() {
    $screens = ['post', 'points-of-sale'];
    foreach ($screens as $screen) {
        add_meta_box(
            'bposcpt_general_info_box',           // Unique ID
            __( 'Point of Sale general info', 'bozu-points-of-sale-cpt' ),  // Box title
            'bposcpt_general_info_box_markup',  // Content callback, must be of type callable
            $screen                   // Post type
        );
    }
}
add_action('add_meta_boxes', 'bposcpt_add_custom_box');



/**
 * Getting Meta Box markup - callback
 *
 * @since      0.0.1-dev
 * @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/#getting-values
 * @link https://developer.wordpress.org/reference/functions/get_post_meta/
 *
 */
function bposcpt_general_info_box_markup( $post ) {

    $web_url = get_post_meta( $post->ID, '_bposcpt_web_url', true );
    $value = get_post_meta($post->ID, '_bposcpt_meta_key', true);

    ?>

    <div class="">
        <label for="bposcpt_web_url"><?php _e( 'Web for more info', 'bozu-points-of-sale-cpt' ) ?></label>
        <input id="bposcpt_web_url" type="url" name="bposcpt_web_url" value="<?php echo get_post_meta( $post->ID, '_bposcpt_web_url', true ) ?>">
    </div>

    <div class="">
        <label for="bposcpt_field">Description for this field</label>
        <select name="bposcpt_field" id="bposcpt_field" class="">
            <option value="">Select something...</option>
            <option value="something" <?php selected($value, 'something'); ?>>Something</option>
            <option value="else" <?php selected($value, 'else'); ?>>Else</option>
        </select>
    </div>
    <?php
}



/**
 * Setting (saving) Meta Box content
 *
 * @since       0.0.1-dev
 * @link        https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/#saving-values
 * @link        https://developer.wordpress.org/reference/functions/update_post_meta/
 * @link        https://developer.wordpress.org/reference/hooks/save_post/
 *
 */
function bposcpt_save_postdata( $post_id ) {
    if ( array_key_exists( 'bposcpt_field', $_POST ) ) {
        update_post_meta(
            $post_id,
            '_bposcpt_meta_key',
            $_POST[ 'bposcpt_field' ]
        );
    }
    if ( array_key_exists( 'bposcpt_web_url', $_POST ) ) {
        update_post_meta(
            $post_id,
            '_bposcpt_web_url',
            $_POST[ 'bposcpt_web_url' ]
        );
    }
}
add_action( 'save_post', 'bposcpt_save_postdata' );
