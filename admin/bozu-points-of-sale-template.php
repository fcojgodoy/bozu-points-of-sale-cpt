<?php
/**
 *
 * @link        http://example.com
 * @since       0.0.1-dev
 *
 * @package     Bozu Points of Sale CPT
 * @subpackage  Bozu Points of Sale CPT/admin
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <ul class="products columns-4">

                <?php
                $query = new WP_Query( array( 'post_type' => 'points-of-sale' ) );

                // Make sure $query is not empty.
                if ( $query->have_posts() ) {
                 
                    // Loop over the results.
                    while ( $query->have_posts() ) {
                        $query->the_post();
                        ?>

                        <li class="product">
                            <article id="post-<?php the_ID(); ?>" >
                                <h3><?php the_title(); ?></h3>
                                <?php the_post_thumbnail( 'medium' ); ?>
                                <?php the_content(); ?>
                                <a href="<?php echo esc_url( get_post_meta( get_the_ID(), '_bposcpt_web_url', true ) ) ?>">
                                    <?php _e( 'Web for more info', 'bozu-points-of-sale-cpt' ) ?>
                                </a>
                                
                                <?php days() ?>
                                
                                <?php echo get_post_meta( get_the_ID(), '_wporg_meta_key', true ) ?>
                            </article>
                        </li>

                        <?php
                    }

                }

                //reset global $post 
                wp_reset_postdata();
                ?>

            </ul>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
do_action( 'storefront_sidebar' );
get_footer();
