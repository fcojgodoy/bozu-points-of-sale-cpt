<?php

if ( ! function_exists('bposcpt_register_cpt') ) {

/**
 * Register Custom Post Type
 *
 * @link https://developer.wordpress.org/plugins/post-types/registering-custom-post-types/
 * @link https://developer.wordpress.org/reference/functions/register_post_type/
 */
function bposcpt_register_cpt() {

	$labels = array(
		'name'                  => __( 'Points of Sale', 'bozu-points-of-sale-cpt' ),
		'singular_name'         => __( 'Point of Sale', 'bozu-points-of-sale-cpt' ),
		'menu_name'             => __( 'Points of Sale', 'bozu-points-of-sale-cpt' ),
		'add_new_item'          => __( 'Add new Point of Sale', 'bozu-points-of-sale-cpt' ),
		'name_admin_bar'        => __( 'Points of Sale', 'bozu-points-of-sale-cpt' ),
		// 'archives'              => __( 'Item Archives', 'bozu-points-of-sale-cpt' ),
		// 'attributes'            => __( 'Item Attributes', 'bozu-points-of-sale-cpt' ),
		// 'parent_item_colon'     => __( 'Parent Item:', 'bozu-points-of-sale-cpt' ),
		// 'all_items'             => __( 'All Items', 'bozu-points-of-sale-cpt' ),
		// 'add_new'               => __( 'Add New', 'bozu-points-of-sale-cpt' ),
		// 'new_item'              => __( 'New Item', 'bozu-points-of-sale-cpt' ),
		// 'edit_item'             => __( 'Edit Item', 'bozu-points-of-sale-cpt' ),
		// 'update_item'           => __( 'Update Item', 'bozu-points-of-sale-cpt' ),
		// 'view_item'             => __( 'View Item', 'bozu-points-of-sale-cpt' ),
		// 'view_items'            => __( 'View Items', 'bozu-points-of-sale-cpt' ),
		// 'search_items'          => __( 'Search Item', 'bozu-points-of-sale-cpt' ),
		// 'not_found'             => __( 'Not found', 'bozu-points-of-sale-cpt' ),
		// 'not_found_in_trash'    => __( 'Not found in Trash', 'bozu-points-of-sale-cpt' ),
		// 'featured_image'        => __( 'Featured Image', 'bozu-points-of-sale-cpt' ),
		// 'set_featured_image'    => __( 'Set featured image', 'bozu-points-of-sale-cpt' ),
		// 'remove_featured_image' => __( 'Remove featured image', 'bozu-points-of-sale-cpt' ),
		// 'use_featured_image'    => __( 'Use as featured image', 'bozu-points-of-sale-cpt' ),
		// 'insert_into_item'      => __( 'Insert into item', 'bozu-points-of-sale-cpt' ),
		// 'uploaded_to_this_item' => __( 'Uploaded to this item', 'bozu-points-of-sale-cpt' ),
		// 'items_list'            => __( 'Items list', 'bozu-points-of-sale-cpt' ),
		// 'items_list_navigation' => __( 'Items list navigation', 'bozu-points-of-sale-cpt' ),
		// 'filter_items_list'     => __( 'Filter items list', 'bozu-points-of-sale-cpt' ),
	);
	$args = array(
		'label'                 => __( 'Point of Sale', 'bozu-points-of-sale-cpt' ),
        'labels'                => $labels,
		'description'           => __( 'Point of Sale custom post type', 'bozu-points-of-sale-cpt' ),
        'public'                => true,
        'hierarchical'          => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_nav_menus'     => true,
        'show_in_admin_bar'     => true,
        'show_in_rest'          => true,
		'menu_position'         => 55,
		'menu_icon'             => 'dashicons-store',
        'capability_type'       => 'page',
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail' ),
        // 'taxonomies'            => array( 'day' ),
		'has_archive'           => true,
        'can_export'            => true,
        // 'rewrite' => array( 'slug' => __( 'points-of-sale', 'bozu-points-of-sale-cpt' ) ),
	);
	register_post_type( 'points-of-sale', $args );
}

add_action('init', 'bposcpt_register_cpt');

}
