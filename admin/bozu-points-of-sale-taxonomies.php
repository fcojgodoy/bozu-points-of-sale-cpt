<?php

/**
 * The taxonomies.
 *
 * @link        http://example.com
 * @since       0.0.1-dev
 *
 * @package     Bozu Points of Sale CPT
 * @subpackage  Bozu Points of Sale CPT/admin
 */

/**
 * Create day taxonomy for Points of Sale CPT
 *
 * @since       0.0.1-dev
 * @link https://developer.wordpress.org/plugins/taxonomies/working-with-custom-taxonomies/
 * @link https://developer.wordpress.org/reference/functions/register_taxonomy/
 */
function bposcpt_register_taxonomy_day() {
    $labels = [
        'name'              => _x( 'Days', 'taxonomy general name', 'bozu-points-of-sale-cpt' ),
        'singular_name'     => _x( 'Day', 'taxonomy singular name', 'bozu-points-of-sale-cpt' ),
        'search_items'      => __( 'Search Days', 'bozu-points-of-sale-cpt' ),
        'all_items'         => __( 'All Days', 'bozu-points-of-sale-cpt' ),
        'parent_item'       => __( 'Parent Day', 'bozu-points-of-sale-cpt' ),
        'parent_item_colon' => __( 'Parent Day:', 'bozu-points-of-sale-cpt' ),
        'edit_item'         => __( 'Edit Day', 'bozu-points-of-sale-cpt' ),
        'update_item'       => __( 'Update Day', 'bozu-points-of-sale-cpt' ),
        'add_new_item'      => __( 'Add New Day', 'bozu-points-of-sale-cpt' ),
        'new_item_name'     => __( 'New Day Name', 'bozu-points-of-sale-cpt' ),
        'menu_name'         => __( 'Days', 'bozu-points-of-sale-cpt' ),
    ];
    $rewrite = array(
        'slug'                       => __( 'days', 'bozu-points-of-sale-cpt' ),
        'with_front'                 => false,
        'hierarchical'               => false,
    );
    $args = [
        'labels'            => $labels,
        'public'            => true,
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'show_in_rest'      => true,
        'rewrite'           => $rewrite,
    ];
    register_taxonomy( 'day', 'points-of-sale', $args );
}
add_action( 'init', 'bposcpt_register_taxonomy_day' );
