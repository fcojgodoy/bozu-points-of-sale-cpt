<?php

function days() {
    //get all terms assigned to this post
    $days = get_the_terms( get_the_ID(), 'day' );
    //if we have terms assigned to this post
    if( $days ) {
      // var_dump( $days );
        echo '<p class="buttons">';
        //loop through each term
        foreach( $days as $day ) {
            //collect term information and display it
            var_dump( $day );
            $day_name = $day->name;
            $day_link = get_term_link( $day );
            echo "<a class='button is-primary is-small' href='$day_link'>
                $day_name
            </a>";
        }
        echo '</p>';
    }
}
