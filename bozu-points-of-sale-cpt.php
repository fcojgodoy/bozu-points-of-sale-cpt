<?php

/**
 * The plugin main file
 *
 * @link              http://example.com
 * @since             0.0.1-dev
 * @package           Bozu Points of Sale CPT
 *
 * @wordpress-plugin
 * Plugin Name: Bozu Points of Sale CPT
 * Plugin URI:  https://github.com/fcojgodoy/#
 * Description: Create custom post type for points of sale
 * Version:     0.0.1-dev
 * Author:      fcojgodoy
 * Author URI:  https://github.com/fcojgodoy/
 * License:     GPL3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: bozu-points-of-sale-cpt
 * Domain Path: /languages
 */



// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}



/**
 * Load plugin textdomain
 *
 * @link https://developer.wordpress.org/reference/functions/load_plugin_textdomain/
 */
function bposcpt_load_plugin_textdomain() {
    load_plugin_textdomain( 'bozu-points-of-sale-cpt', FALSE, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'init', 'bposcpt_load_plugin_textdomain' );



/**
 * Refresh WordPress permalinks when the plugin is activate
 *
 * @link https://developer.wordpress.org/plugins/the-basics/activation-deactivation-hooks/
 */
function bposcpt_activate() {
    // trigger our function that registers the custom post type
    bposcpt_register_cpt();

    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'bposcpt_activate' );



/**
 * Refresh WordPress permalinks when the plugin is desactivate
 *
 * @link https://developer.wordpress.org/plugins/the-basics/activation-deactivation-hooks/
 */
function bposcpt_deactivate() {
    // our post type will be automatically removed, so no need to unregister it

    // clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'bposcpt_deactivate' );



/**
 * Requering files
 *
 */
if ( is_admin() ) {
    // we are in admin mode
    require plugin_dir_path( __FILE__ ) . 'admin/bozu-points-of-sale-meta-boxes.php';
}

require plugin_dir_path( __FILE__ ) . 'admin/bozu-points-of-sale-add-cpt.php';
require plugin_dir_path( __FILE__ ) . 'admin/bozu-points-of-sale-taxonomies.php';
require plugin_dir_path( __FILE__ ) . 'admin/bozu-points-of-sale-add-templates.php';
require plugin_dir_path( __FILE__ ) . 'controllers/bozu-points-of-sale-templates.php';
